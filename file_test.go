package isofs

import (
	"io"
	"testing"
)

func TestFileRead(t *testing.T) {
	// pretty simple: never should be able to write as it is a read-only fs
	// we use
	f, content := GetTestFile(t)

	b := make([]byte, 20, 20)
	read, err := f.Read(b)
	if read != 0 && err != io.EOF {
		t.Errorf("received unexpected error when reading: %v", err)
	}
	if read != len(content) {
		t.Errorf("read %d bytes instead of expected %d", read, len(content))
	}
	bString := string(b[:read])
	if bString != content {
		t.Errorf("Mismatched content:\nActual: '%s'\nExpected: '%s'", bString, content)
	}
}
