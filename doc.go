// Package isofs provides an fs.FS interface to a pre-mastered ISO 9660 fs
// It understands basic ISO 9660 filesystems as well as ones with RockRidge extensions.
// It is based on the ISO9660 part of the go-diskfs library by Avi Deitcher at
// https://github.com/diskfs/go-diskfs.
//
// Joliet and UDF are not currently supported.  Support may be added in later versions
// of this library.
//
// Reference documentation
//
//	ISO9660 https://wiki.osdev.org/ISO_9660
//	ISO9660 / ECMA-119 http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-119.pdf
//	System Use Sharing Protocol http://cdrtools.sourceforge.net/private/RRIP/susp.ps
//	Rock Ridge http://cdrtools.sourceforge.net/private/RRIP/rrip.ps
package isofs
