package isofs

import (
	"io/fs"
)

// Directory represents a single directory in a FAT32 fs
type testDirectory struct {
	fs.DirEntry
	entries []*inode
}

// dirEntriesFromBytes loads the directory entries from the raw bytes
func (d *testDirectory) entriesFromBytes(b []byte, f *FS) error {
	entries, err := parseDirEntries(b, f)
	if err != nil {
		return err
	}
	d.entries = entries
	return nil
}
