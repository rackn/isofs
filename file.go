package isofs

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"syscall"
)

// File represents a single file in an iso9660 fs.
type File struct {
	*inode
	offset int64
}

var (
	_ = fs.File(&File{})
	_ = fs.ReadDirFile(&File{})
)

func (fl *File) ReadAt(b []byte, offset int64) (readsize int, err error) {
	if fl.isSubdirectory() {
		return 0, syscall.EISDIR
	}
	var n int
	for i := 0; i < len(fl.extents); i++ {
		if offset > fl.extents[i].size {
			offset -= fl.extents[i].size
			continue
		}
		n, err = fl.extents[i].readAt(fl.fs.file, offset, b)
		readsize += n
		if err != nil || n == len(b) {
			return
		}
		b = b[n:]
	}
	return readsize, io.EOF
}

// Read reads up to len(b) bytes from the File.
// If the file is actually a directory, Read returns 0, syscall.EISDIR
// It returns the number of bytes read and any error encountered.
// At end of file, Read returns 0, io.EOF
//
// reads from the last known offset in the file from last read or write
// use Seek() to set at a particular point
func (fl *File) Read(b []byte) (int, error) {
	readSize, err := fl.ReadAt(b, fl.offset)
	fl.offset += int64(readSize)
	return readSize, err
}

// Closes closes a File.
func (fl *File) Close() error {
	return nil
}

// Seek set the offset to a particular point in the file
func (fl *File) Seek(offset int64, whence int) (int64, error) {
	if fl.inode == nil {
		return 0, syscall.EISDIR
	}
	newOffset := int64(0)
	switch whence {
	case io.SeekStart:
		newOffset = offset
	case io.SeekEnd:
		newOffset = int64(fl.Size()) + offset
	case io.SeekCurrent:
		newOffset = fl.offset + offset
	}
	if newOffset < 0 {
		return fl.offset, fmt.Errorf("Cannot set offset %d before start of file", offset)
	} else if newOffset > int64(fl.Size()) {
		return fl.offset, fmt.Errorf("Cannot set offset %d after end of file", offset)
	}
	fl.offset = newOffset
	return fl.offset, nil
}

// Stat returns stat information for a File
func (fl *File) Stat() (os.FileInfo, error) {
	return fl.inode, nil
}

// ReadDir returns an array of []fs.FileInfo entries.
// If the File is not a directory, Readdir returns 0, io.EOF
//
// Otherwise, Readdir behaves the same way as os.File.Readdir
func (fl *File) ReadDir(count int) ([]fs.DirEntry, error) {
	if !fl.isSubdirectory() {
		return nil, fs.ErrInvalid
	}
	if fl.offset >= int64(len(fl.dirEntries)) {
		return nil, io.EOF
	}
	if count <= 0 {
		fl.offset = 0
		return fl.dirEntries, nil
	}
	if int(fl.offset)+count > len(fl.dirEntries) {
		count = len(fl.dirEntries) - int(fl.offset)
	}
	res := fl.dirEntries[int(fl.offset):count]
	fl.offset += int64(count)
	return res, nil
}
