package isofs

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"strings"
	"syscall"
)

const (
	volumeDescriptorSize = 2 * KB  // each volume descriptor is 2KB
	systemAreaSize       = 32 * KB // 32KB system area size
	defaultSectorSize    = 2 * KB
)

// Src is something we can create a Filesystem from.  Anything
// that satisfies fs.File and io.ReaderAt is a valid ISO src.
type Src interface {
	fs.File
	io.ReaderAt
}

// FS gives access to the files in an ISO.
// It satisfies the fs.FS, fs.ReadDirFS, and fs.StatFS
// interfaces.
type FS struct {
	volumes        volumeDescriptors
	file           Src
	rootDir        *inode
	suspExtensions []suspExtension
	size           int64
	blocksize      int64
	suspEnabled    bool
	suspSkip       uint8
}

var (
	_ = fs.FS(&FS{})
	_ = fs.ReadDirFS(&FS{})
	_ = fs.StatFS(&FS{})
)

// Equal compare if two filesystems are equal
func (iso *FS) Equal(a *FS) bool {
	localMatch := iso.file == a.file && iso.size == a.size
	return localMatch
}

// Close the fs by closing the backing File.
// You should not use anything from this FS afterwards.
func (iso *FS) Close() error {
	return iso.file.Close()
}

// Read reads a fs from a given file, which must be open.
func Open(file Src) (*FS, error) {
	size := int64(0)
	if st, err := file.Stat(); err != nil {
		return nil, err
	} else {
		size = st.Size()
	}

	// at bare minimum, it must have enough space for the system area, one volume descriptor, one volume decriptor set terminator, and one block of data
	if size < systemAreaSize+2*volumeDescriptorSize+defaultSectorSize {
		return nil, fmt.Errorf("requested size is too small to allow for system area (%d), one volume descriptor (%d), one volume descriptor set terminator (%d), and one block (%d)", systemAreaSize, volumeDescriptorSize, volumeDescriptorSize, defaultSectorSize)
	}

	/*
		    // read system area
			systemArea := make([]byte, systemAreaSize)
			n, err := file.ReadAt(systemArea, 0)
			if err != nil {
				return nil, fmt.Errorf("Could not read bytes from file: %v", err)
			}
			if uint16(n) < uint16(systemAreaSize) {
				return nil, fmt.Errorf("Only could read %d bytes from file", n)
			}
			// we do not do anything with the system area for now
	*/

	// next read the volume descriptors, one at a time, until we hit the terminator
	var (
		pvd *primaryVolumeDescriptor
		vd  volumeDescriptor
		vds []volumeDescriptor
	)
done:
	for i := 0; ; i++ {
		if vdBytes, err := readBytes(file, systemAreaSize+int64(i)*volumeDescriptorSize, volumeDescriptorSize); err != nil {
			return nil, fmt.Errorf("Unable to read bytes for volume descriptor %d: %v", i, err)
		} else if vd, err = volumeDescriptorFromBytes(vdBytes); err != nil {
			return nil, fmt.Errorf("Error reading Volume Descriptor: %v", err)
		}
		// is this a terminator?
		switch vd.Type() {
		case volumeDescriptorTerminator:
			break done
		case volumeDescriptorPrimary:
			pvd = vd.(*primaryVolumeDescriptor)
		}
		vds = append(vds, vd)
	}

	// load up our path table and root directory entry
	var (
		rootDirEntry *inode
	)
	blocksize := int64(pvd.blocksize)
	if pvd != nil {
		rootDirEntry = pvd.rootDirectoryEntry
	}

	var (
		suspEnabled  bool
		skipBytes    uint8
		suspHandlers []suspExtension
	)
	// is system use enabled?
	location := rootDirEntry.extents[0]
	if b, err := location.read(file, 0, 1); err != nil {
		return nil, fmt.Errorf("Unable to read root directory size at location %d: %v", location, err)
	} else if b, err = location.read(file, 0, int(b[0])); err != nil {
		return nil, fmt.Errorf("Unable to read root directory entry at location %d: %v", location, err)
	} else if de, err := parseDirEntry(b, &FS{
		suspEnabled: true,
		file:        file,
		blocksize:   blocksize,
	}); err != nil {
		return nil, fmt.Errorf("Error parsing root entry from bytes: %v", err)
	} else {
		// is the SUSP in use?
		for _, ext := range de.extensions {
			if s, ok := ext.(directoryEntrySystemUseExtensionSharingProtocolIndicator); ok {
				suspEnabled = true
				skipBytes = s.SkipBytes()
			}

			// register any extension handlers
			if s, ok := ext.(directoryEntrySystemUseExtensionReference); suspEnabled && ok {
				extHandler := getRockRidgeExtension(s.ExtensionID())
				if extHandler != nil {
					suspHandlers = append(suspHandlers, extHandler)
				}
			}
		}
	}

	iso := &FS{
		size: size,
		file: file,
		volumes: volumeDescriptors{
			descriptors: vds,
			primary:     pvd,
		},
		blocksize:      blocksize,
		rootDir:        rootDirEntry,
		suspEnabled:    suspEnabled,
		suspSkip:       skipBytes,
		suspExtensions: suspHandlers,
	}
	rootDirEntry.fs = iso
	return iso, iso.rootDir.readDir()
}

// Label returns the label assigned to the fs
func (iso *FS) Label() string {
	if iso.volumes.primary == nil {
		return ""
	}
	return iso.volumes.primary.volumeIdentifier
}

func processSymlink(parts []string, offset int, sl string) ([]string, error) {
	if sl[0] == '/' {
		sl = sl[1:]
	} else {
		sl = path.Join(
			path.Join(parts[:offset]...),
			sl)
	}
	if offset < len(parts) {
		sl = path.Join(sl, path.Join(parts[offset+1:]...))
	}
	if !fs.ValidPath(sl) {
		return nil, fs.ErrInvalid
	}
	return strings.Split(sl, "/"), nil

}

func (iso *FS) getDent(p string, followSymlink bool) (ent *inode, err error) {
	if !fs.ValidPath(p) {
		err = fs.ErrInvalid
		return
	}
	parts := strings.Split(p, "/")
	if parts[0] == "." {
		return iso.rootDir, nil
	}
	for count := 128; count > 0; count-- {
		var offset int
		if ent == nil {
			ent = iso.rootDir
		}
		if ent, offset, err = ent.getDent(parts, offset); err != nil {
			return
		}
		if offset < len(parts) {
			// if we stop before processing all the parts, either:
			// * the file is not found, or
			// * we need to process a symbolic link.
			if sl := ent.symLink(); sl != "" {
				if parts, err = processSymlink(parts, offset, sl); err != nil {
					return
				}
				continue
			}
			err = os.ErrNotExist
			return
		}
		sl := ent.symLink()
		if sl == "" || !followSymlink {
			return
		}
		if parts, err = processSymlink(parts, offset-1, sl); err != nil {
			return
		}
	}
	err = syscall.ETOOMANYREFS
	return
}

func pathifyErr(op, p string, err error) error {
	if err == nil {
		return nil
	}
	return &fs.PathError{Op: op, Path: p, Err: err}
}

func (iso *FS) Stat(p string) (fi fs.FileInfo, err error) {
	fi, err = iso.getDent(p, true)
	err = pathifyErr("stat", p, err)
	return
}

func (iso *FS) Lstat(p string) (fi fs.FileInfo, err error) {
	fi, err = iso.getDent(p, false)
	err = pathifyErr("lstat", p, err)
	return
}

func (iso *FS) ReadLink(p string) (sl string, err error) {
	var fi *inode
	if fi, err = iso.getDent(p, false); err == nil {
		sl, err = fi.ReadLink()
	}
	err = pathifyErr("readlink", p, err)
	return
}

// Open opens an fs.File contained in the FS.
func (iso *FS) Open(p string) (fs.File, error) {
	ent, err := iso.getDent(p, true)
	if err != nil {
		return nil, pathifyErr("open", p, err)
	}
	res := &File{inode: ent}
	return res, nil
}

// ReadDir - read directory entry on iso only (not workspace)
func (iso *FS) ReadDir(p string) ([]fs.DirEntry, error) {
	ent, err := iso.getDent(p, true)
	if err == nil {
		if ent.isSubdirectory() {
			return ent.dirEntries, nil
		}
		err = fs.ErrInvalid
	}
	return nil, pathifyErr("readdir", p, err)
}
